import { productReducer } from './store/product/reducers';
import { ProductState } from './store/product/state';
import { environment } from './../environments/environment.prod';
import { CartState } from './store/cart/state';
import { cartStateReducer } from './store/cart/reducers';
import { combineReducers, ActionReducer } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';
import { storeFreeze } from 'ngrx-store-freeze';

export interface AppState {
  cart: CartState,
  products: ProductState
}

const reducers = {
  cart: cartStateReducer,
  products: productReducer
}


export const developmentReducer: ActionReducer<AppState> = compose (storeFreeze, combineReducers)(reducers);
export const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}
