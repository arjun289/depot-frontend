import { Cart } from './../../models/cart';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { CartActions } from './../cart/actions';
import { CartService } from './../../core/services/cart.service';

import { Effect, Actions, toPayload } from '@ngrx/effects';
import {  Injectable } from '@angular/core';

@Injectable()
export class CartEffects {

  constructor(private actions$: Actions,
              private cartService: CartService,
              private cartActions: CartActions ) { }

  // tslint:disable-next-line:member-ordering
  @Effect()
    getCart$: Observable<Action> = this.actions$.
    ofType(CartActions.GET_CART)
    .switchMap((action: Action) => this.cartService.getCart())
    .map((data: Cart) => this.cartActions.changeCartData(data));

    // tslint:disable-next-line:member-orderin
    // tslint:disable-next-line:member-ordering
    @Effect()
    addToCart$: Observable<Action> = this.actions$.
    ofType(CartActions.ADD_TO_CART)
    .switchMap((action: Action) => this.cartService.addToCart(action.payload))
    .map((data: Cart) => this.cartActions.changeCartData(data));

    // tslint:disable-next-line:member-ordering
    @Effect()
    emptyCart$: Observable<Action> = this.actions$.
    ofType(CartActions.EMPTY_CART)
    .switchMap((action: Action) => this.cartService.emptyCart())
    .map((data: Cart) => this.cartActions.changeCartData(data));

}


