import { LineItem } from './../../models/lineitem';

export interface CartState {
  id: number;
  line_items: LineItem[];
  cart_total: number;
}

export const CartInitialState: CartState = {
  id : null,
  line_items: [],
  cart_total: 0
}
