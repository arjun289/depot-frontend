import { CartActions } from './actions';
import { CartState, CartInitialState } from './state';
import { ActionReducer, Action } from '@ngrx/store';

export const cartStateReducer: ActionReducer<CartState> =
  (state = CartInitialState, action: Action ) => {
    switch (action.type) {
      case CartActions.CHANGE_CART_DATA: {
        return Object.assign({}, action.payload );
      }
      default: return state;
    }
  }

