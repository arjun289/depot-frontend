
import { Cart } from './../../models/cart';
import { Action } from '@ngrx/store';

export class CartActions {
  static GET_CART = 'GET_CART';
  static ADD_TO_CART = 'ADD_TO_CART';
  static EMPTY_CART = 'EMPTY_CART';
  static CHANGE_CART_DATA = 'CHANGE_CART_DATA';

  getCart(): Action {
    return {
      type: CartActions.GET_CART,
    }
  }

  changeCartData(cart: Cart): Action {
    return {
      type: CartActions.CHANGE_CART_DATA,
      payload: cart
    }
  }

  addToCart(productId: number): Action {
    return {
      type: CartActions.ADD_TO_CART,
      payload: productId
    }
  }

  emptyCart(): Action {
    return {
      type: CartActions.EMPTY_CART,
    }
  }

}
