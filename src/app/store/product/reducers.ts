import { ProductActions } from './actions';
import { ProductState, ProductInitialState } from './state';
import { ActionReducer, Action } from '@ngrx/store';

export const productReducer: ActionReducer<ProductState> =
  (state = ProductInitialState, action: Action) => {
    switch ( action.type ) {
      case ProductActions.GET_PRODUCTS_SUCCESS: {
        console.log('payload', action.payload);
        const products = action.payload;
        return Object.assign({}, state, {products: products} );
      }
      default: return state;
    }
  }
