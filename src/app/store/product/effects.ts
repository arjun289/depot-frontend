import { Product } from './../../models/product';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ProductActions } from './actions';
import { ProductService } from './../../core/services/product.service';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Injectable } from '@angular/core';


@Injectable()

export class ProductEffects {

  constructor (
               private actions$: Actions,
               private productService: ProductService,
               private productActions: ProductActions) {}

  // tslint:disable-next-line:member-ordering
  @Effect()
    getProduct$: Observable<Action> = this.actions$
      .ofType(ProductActions.GET_PRODUCTS)
      .switchMap((action: Action) => this.productService.getProducts())
      .map((data: Product[]) => this.productActions.getProductsSuccess(data));
}
