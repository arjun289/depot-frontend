import { Product } from './../../models/product';
import { Action } from '@ngrx/store';

export class ProductActions {
  static GET_PRODUCTS = 'GET_PRODUCTS';
  static GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';

  getProducts(): Action {
    return {
      type: ProductActions.GET_PRODUCTS
    }
  }

  getProductsSuccess(products: Product[]): Action {
    return {
      type: ProductActions.GET_PRODUCTS_SUCCESS,
      payload: products
    }
  }
}
