import { LineItem } from './lineitem';

export class Cart {
  id: number;
  line_items: LineItem[];
  cart_total: number;
}
