import { Observable } from 'rxjs/Observable';
import { ProductActions } from './../store/product/actions';
import { AppState } from './../app.reducers';
import { Store } from '@ngrx/store';
import { environment as env} from './../../environments/environment';
import { Product } from './../models/product';
import { ProductService } from './../core/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  apiUrl = env.apiUrl;
  products: Product[];

  constructor(private store: Store<AppState>, private actions: ProductActions) {
    this.store.select('products')
    .subscribe((data) => {
      this.products = data['products'];
      console.log('checkdata', data);
    });
   }

  ngOnInit() {
    this.store.dispatch(this.actions.getProducts());
  }

}
