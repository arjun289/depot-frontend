import { CartActions } from './../../../store/cart/actions';
import { CartState } from './../../../store/cart/state';
import { Store } from '@ngrx/store';
import { CartService } from './../../../core/services/cart.service';
import { LineItemService } from './../../../core/services/line-item.service';
import { environment as env} from './../../../../environments/environment';
import { Product } from './../../../models/product';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss']
})
export class ProductListItemComponent implements OnInit {

  @Input() product: Product;
  @Output() addToCartEvent: EventEmitter<number> = new EventEmitter<number>();
  apiUrl = env.apiUrl;

  constructor( private store: Store<CartState>, private actions: CartActions) { }

  ngOnInit() {
  }

  addToCart( productid: number) {
    this.store.dispatch(this.actions.addToCart(productid));
  }

}
