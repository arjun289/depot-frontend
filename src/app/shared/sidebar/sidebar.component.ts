import { AppState } from './../../app.reducers';
import { Observable } from 'rxjs/Observable';
import { CartActions } from './../../store/cart/actions';
import { CartState } from './../../store/cart/state';
import { LineItem } from './../../models/lineitem';
import { Cart } from './../../models/cart';
import { CartService } from './../../core/services/cart.service';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as cartReducer from './../../store/cart/reducers';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SidebarComponent implements OnInit {

   cartData: Observable<Cart>
   lineItem: LineItem[];

  constructor(private store: Store<AppState>, private actions: CartActions) {
    this.cartData = this.store.select('cart');
  }

  ngOnInit() {
    this.store.dispatch(this.actions.getCart());
  }

  emptyCart() {
    this.store.dispatch({ type: CartActions.EMPTY_CART })
  }

}
