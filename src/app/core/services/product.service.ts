import { environment as env } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


@Injectable()
export class ProductService {

  constructor( private http: Http) {
  }

  getProducts() {
   const url = `${env.apiUrl}/products.json`;
   return this.http.get(url).map((res) => {
    const products = res.json();
    return products;
   })
  }

  getProduct() {
  }

}
