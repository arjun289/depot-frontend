import { environment as env } from './../../../environments/environment';
import { Http, RequestOptions, Headers   } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LineItemService {

  constructor( private http: Http ) { }

  sendLineItem ( productId: number) {
    const url = `${env.apiUrl}/line_items.json`;
    const body = { cart_id: localStorage.getItem('CartId'), product_id: productId }

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post (url, body, options).map((res) => {
      const products = res.json();
      return products;
    })

  }
}

