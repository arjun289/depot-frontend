import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Cart } from './../../models/cart';
import { environment as env } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';


@Injectable()
export class CartService {

  // cartData = new Subject<Cart>(); used before store implementation

  constructor( private http: Http ) { }

  getCart() {
    const cartId = localStorage.getItem('CartId') || null;
    const url = `${env.apiUrl}/carts/current_cart/${cartId}`;
    return this.http.get(url).map((res) => {
      const cart = res.json();
      // this.cartData.next(cart);
      localStorage.setItem('CartId', cart.id);
      return cart;
    })
  }

  addToCart ( productId: number) {
    const url = `${env.apiUrl}/line_items.json`;
    const body = { cart_id: localStorage.getItem('CartId'), product_id: productId }

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post (url, body, options).map((res) => {
      const cart = res.json();
      // this.cartData.next(cart);
      return cart;
    })
  }

  removeFromCart() {
  }

  emptyCart() {
    const cartId = localStorage.getItem('CartId');
    const url = `${env.apiUrl}/carts/empty_cart/${cartId}`;

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.delete(url).map((res) => {
      const cart = res.json();
      return cart;
    })
  }


}
