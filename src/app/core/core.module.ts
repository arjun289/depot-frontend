import { CartService } from './services/cart.service';
import { LineItemService } from './services/line-item.service';
import { HttpModule } from '@angular/http';
import { ProductService } from './services/product.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  providers: [
    ProductService,
    LineItemService,
    CartService
  ],
  declarations: []
})
export class CoreModule { }
