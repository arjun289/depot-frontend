import { ProductActions } from './store/product/actions';
import { ProductEffects } from './store/product/effects';
import { CartActions } from './store/cart/actions';
import { CartEffects } from './store/cart/effects';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { counterReducer } from './counter';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducer } from './app.reducers';
import { EffectsModule } from '@ngrx/effects';

// rxjs operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    CoreModule,
    EffectsModule.run(CartEffects),
    EffectsModule.run(ProductEffects),
    StoreModule.provideStore( reducer ),
    StoreDevtoolsModule.instrumentStore({
      maxAge: 5
    })
  ],
  providers: [
    CartActions,
    ProductActions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
