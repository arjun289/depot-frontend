import { DepotFrontendPage } from './app.po';

describe('depot-frontend App', () => {
  let page: DepotFrontendPage;

  beforeEach(() => {
    page = new DepotFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
